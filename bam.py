#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
from utils import bipolar, clip


class BAM:
    @staticmethod
    def get_BAM(couples, fseuil=np.sign):
        n, k = [len(v) for v in couples[0]]
        bam = BAM(n, k, fseuil)
        bam.M = sum(np.outer(bipolar(A), bipolar(B)) for A, B in couples)
        return bam

    def __init__(self, n, k, fseuil=np.sign):
        self.M  = np.zeros((n, k), int)
        self.fseuil = fseuil

    def add_couple(self, A, B):
        X, Y = bipolar(A), bipolar(B)
        self.M  += np.outer(X, Y)

    def _retrieveA(self, B):
        Y = bipolar(B)
        X = self.fseuil(np.inner(Y, self.M))
        while True:
            V = self.fseuil(np.inner(X, self.M.T))
            U = self.fseuil(np.inner(V, self.M))
            if np.array_equal(X, U) and np.array_equal(Y, V):
                break
            X, Y = U, V
        return X, Y

    def _retrieveB(self, A):
        X = bipolar(A)
        Y = self.fseuil(np.inner(X, self.M.T))
        while True:
            U = self.fseuil(np.inner(Y, self.M))
            V = self.fseuil(np.inner(U, self.M.T))
            if np.array_equal(X, U) and np.array_equal(Y, V):
                break
            X, Y = U, V
        return X, Y

    def retrieveA(self, B):
        return clip(self._retrieveA(B)[0])

    def retrieveB(self, A):
        return clip(self._retrieveB(A)[1])

