#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
from utils import bipolar, clip
from bam import BAM



def Energy(X, Y, M):
    return -np.sum(M * np.outer(bipolar(X), bipolar(Y)))

class MBS:
    DEBUG = 0
    
    @staticmethod
    def get_MBS(couples):
        n, k = [len(v) for v in couples[0]]
        mbs = MBS(n, k)
        for A, B in couples:
            mbs.add_couple(A, B)
        return mbs

    def __init__(self, n, k):
        self.bams_lst = [BAM(n, k)]
        self.n = n
        self.k = k
        self.E = -(n * k)

    def add_couple(self, A, B):
        AB = np.outer(bipolar(A), bipolar(B))
        for bam in self.bams_lst:
            bam.M += AB
            if Energy(A, B, bam.M) == self.E:
                return
            bam.M -= AB
        new_bam = BAM(self.n, self.k)
        new_bam.M += AB
        self.bams_lst.append(new_bam)

    def retrieveB(self, A):
        _A = bipolar(A)
        candidates = [bam._retrieveB(A) for bam in self.bams_lst]
        energy = [(Energy(X, Y, bam.M), Energy(_A, Y, bam.M))
                       for bam, (X, Y) in zip(self.bams_lst, candidates)]
        #for c in candidates: print('  ', c)
        #print(energy)
        filtered = [(Y, e) for (X, Y), (e, _e) in zip(candidates, energy)
                                if e == _e]
        if MBS.DEBUG:
            print(energy)
            #print(filtered)
        Y = min(filtered, key=lambda t: abs(t[1] - self.E))[0]
##        if MBS.DEBUG:
##            print('  got', Y)
        return clip(Y)

    def retrieveA(self, B):
        _B = bipolar(B)
        candidates = [bam._retrieveA(B) for bam in self.bams_lst]
        energy = [(Energy(X, Y, bam.M), Energy(X, _B, bam.M))
                       for bam, (X, Y) in zip(self.bams_lst, candidates)]
        #for c in candidates: print('  ', c)
        #print(energy)
        filtered = [(X, e) for (X, Y), (e, _e) in zip(candidates, energy)
                                if e == _e]
        if MBS.DEBUG:
            print(energy)
            #print(filtered)
        X = min(filtered, key=lambda t: abs(t[1] - self.E))[0]
##        if MBS.DEBUG:
##            print('  got', X)
        return clip(X)

    nbams = property(fget=lambda self: len(self.bams_lst))
