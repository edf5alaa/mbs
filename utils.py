#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from math import exp
import numpy as np

def sigma(x, gamma=1):
    return 1 / (1 + exp(gamma * -x))

def clip(v):
    return np.clip(v, 0, 1)

def _bipolar(x):
    assert x in {-1, 0, 1}
    if x == 1:
        return 1
    return -1

bipolar = np.vectorize(_bipolar)

def rand_vec(sz):
    return np.random.randint(2, size=sz)
