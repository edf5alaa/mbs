#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import random
import numpy as np
from mbs import MBS
from utils import rand_vec, bipolar

data = [
    ([1,0,0], [0,0,1]),
    ([0,1,0], [0,1,0]),
    ([0,0,1], [1,0,0])]

data_test1 = [
    ([1,0,1,0,1,0,1,0,1,0,1,0,1,0,1], [1,1,1,1,0,0,0,0,1,1]),
    ([1,1,0,0,1,1,0,0,1,1,0,0,1,1,0], [1,1,1,0,0,0,1,1,1,0]),
    ([1,1,1,0,0,0,1,1,1,0,0,0,1,1,1], [1,1,0,0,1,1,0,0,1,1]),
    ([1,1,1,1,0,0,0,0,1,1,1,1,0,0,0], [1,0,1,0,1,0,1,0,1,0])]

data_test2 = [
    ([1,0,1,0,1,0], [1,1,0,0]),
    ([1,1,1,0,0,0], [1,0,1,0])]

data_test3 = [
    ([1,0,1,0,1,0,0,0], [0,1,0,0,0,0,0,1,0,0,0,1]),
    ([0,1,0,0,0,1,0,0], [1,0,0,1,0,0,0,0,0,0,0,0]),
    ([0,0,1,0,0,0,1,0], [0,0,1,0,0,1,0,0,1,0,0,0]),
    ([0,0,0,1,0,0,0,1], [0,0,0,0,1,0,1,0,0,1,0,0]),
    ([0,1,0,0,1,0,0,1], [0,0,0,0,1,0,1,0,0,0,1,1])]


data_test4 = [(rand_vec(50), rand_vec(50)) for _ in range(100)]


def noize(v, n=1):
    v = v.copy()
    for i in random.sample(range(len(v)), n):
        v[i] ^= 1
    return v

data_test = data_test4
mbs = MBS.get_MBS(data_test)
MBS.DEBUG = 0
print('used:', mbs.nbams, 'BAMs')
input('?')
#assert all(bam.check() for bam in mbs.bams_lst)

nerrors, nfails = 0, 0
noize_quant = 1

for A, B in data_test:
    #print(bipolar(A), bipolar(B))
    try:
        a = mbs.retrieveA(noize(B, noize_quant), A)
        a_ok = np.array_equal(A, a)
        print('OK' if a_ok else 'KO')
        if not a_ok:
            print('  A:', A, '!=', a)
            nerrors += 1
    except Exception as err:
        print('FAIL! :', repr(err))
        nfails += 1

for A, B in data_test:
    #print(bipolar(A), bipolar(B))
    try:
        b = mbs.retrieveB(noize(A, noize_quant), B)
        b_ok = np.array_equal(B, b)
        print('OK' if b_ok else 'KO')
        if not b_ok:
            print('  B:', B, '!=', b)
            nerrors += 1
    except Exception as err:
        print('FAIL!', repr(err))
        nfails += 1

print(nerrors, 'errors', ',', nfails, 'fails')
